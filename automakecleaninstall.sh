#!/bin/sh

for file in *; do
	if [ -d "$file" ]; then
		echo "confirm compiling $file (y) "
		read -rn1 compile
		if [ "$compile" != "y" ]; then
			continue
		fi
		echo "running 'make clean install' inside $file..."
		cd "$file" &&
			sudo make clean install &&
			cd .. &&
			echo "\n################ done compiling $file ################\n" ||
			echo "\n!!!!!!!!!!!!!!!! failed compiling $file !!!!!!!!!!!!!!!!\n"
	fi
	echo
done
