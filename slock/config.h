/* user and group to drop privileges to */
static const char *user  = "tobi";
static const char *group = "users";

static const char *colorname[NUMCOLS] = {
	[INIT] =   "black",     /* after initialization */
	[INPUT] =  "#331a00",   /* during input */
	[FAILED] = "#662222",   /* wrong password */
};

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 1;
